React Widget Framework
======================
> A simple framework to build embeddable widgets built in React based on an ejected [create-react-app](https://github.com/facebook/create-react-app).

## Quick Overview
React Widget Framework is designed to be a simple, almost configuration free framework for building widgets with React which can easily be embedded into CMS's like Wordpress or Drupal. It is based on an ejected version of [create-react-app](https://github.com/facebook/create-react-app), but adds a simple way to build multiple entry points. This is a work in progress with the eventual goal of removing all configuration and some more advanced optimizations.

### Scripts
This framework comes with three simple scripts already setup and configured for you.

#### `yarn start` or `npm start`
Starts your local development server on port 3000. The port can be overridden by specifying a PORT environment variable.

#### `yarn build` or `npm run build`
Generates an optimized build for production.

#### `yarn test` or `npm test`
Runs your tests in watch mode. You can also pass flags through to have it run code coverage like so: `yarn test --coverage` or `npm test --coverage`.

## Creating a new widget
Widgets have a relatively simple folder structure which contains the following:

```shell
Demo
|- Demo.js # Your main component (required)
|- Demo.test.js # Tests are optional :(
|- index.js # Your entry point (required)
|- index.test.js # Also, still optional :(
|- README.md # Also optional, but will help others
|- webpack.config.js # Build configurations (required)
```

At a minimum, you will need an entry file, a root component, and a build file. Entry file will just import react and your root component and render it into the dom. The build config (`webpack.config.js`) will tell the framework what your entry is, what it's name is, and how to generate some local development files so you can take advantage of the already configured webpack dev server with hot module replacement.

### Widget `webpack.config.js`
You can customize some things about how the widgets get built via your widget's `webpack.config.js`. Some of these properties are required while some are optional. Here are all the configurations that are currently supported.

```javascript
{
	// The entry point is *required*. This tells webpack where
	// your entry is and what the name of the generated
	// file is called. This would generate a file with the
	// naming convention [entry_name].[hash].js.
	// e.g. demo.98aa19188addd2dfd3ed.js
	//
	// The name of your entry *must* be unique across all
	// widgets. All your entries will be combined into a
	// single webpack config when it is built.
	entry: {
		demo: path.posix.resolve('./src/Demo/index.js')
	},
	// All of these options are optional but if you want to do 
	// local development then it is recommended you fill in 
	// filename and mount_point at a minimum. These options
	// only affect local development and have 0 effect on a
	// production build.
	public: {
		// Webpage title
		title: 'My Custom Widget'
		// filename to generate for the HTMLWebpackPlugin
		// and will be hosted by Webpack's dev server.
		// If you specify `demo/index.html`, you can view it at
		// http://localhost:{port}/demo/index.html, where port is
		// 3000 or whatever you specify for the PORT environment
		// variable. Vendor scripts and your entry will be added.
		// You will also need to specify the mount_point for local
		// development.
		filename: 'demo/index.html',
		// Mounting point for you root react component. In your 
		// entry file, you are probably calling something like
		// ReactDOM.render(<Widget />, HTMLElement); Specifying
		// the mount_point will add a <div id='demo-widget'></div>
		// to the dom so your widget has something to mount to.
		mount_point: 'demo-widget',
	}
}
```

## Shared code
Convention for shared code is to put them in the `shared` folder. Common chunks are managed with webpack 4's new `SplitChunksPlugin` and it will pull in any module used in two or more entry points.

## Testing
To run tests, run `yarn test` from the command line. To run tests with code coverage, run `yarn test --coverage`.

Tests use the [Jest](https://facebook.github.io/jest/) framework and run automatically on everything that matches `<rootDir>/src/**/?(*.)(spec|test).{js|jsx}`. Code coverage will include coverage for anything matching `src/**/*.{js,jsx}`. The tests run with `--watch` enabled by default. Only when you are asking for code coverage or when the `CI` environment variable is set will the tests not run in `--watch` mode.
