const resolve = require('./utils/resolve');
const fetch = require('node-fetch');
const fs = require('fs');
const {
	introspectionQuery,
	buildClientSchema,
	printSchema
} = require('graphql/utilities');

// Config to setup the schema generator
let config = {
	graphql_url: 'https://search-now.medstarhealth.org/graphql',
	output_filename: 'medstar-now.graphql',
	project_id: 'MedStarNow'
};

// There is some weird hack that needs to happen to get realy-compiler
// to work, this needs to be looked into at a later date
let string_hack = `
""" Some weird hack to make relay-compiler work"""
type ${config.project_id} {
	id: ID
}
`;

fetch(config.graphql_url, {
	headers: { 'content-type': 'application/json', 'accept': 'application/json' },
	body: JSON.stringify({ query: introspectionQuery }),
	method: 'POST'
})
.then(response => response.json())
.then(response => {
	const schema = printSchema(buildClientSchema(response.data));
	fs.writeFileSync(resolve(config.output_filename), schema + string_hack);
})
.catch(error => console.error('Unable to generate client schema', error));
