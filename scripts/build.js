// Force the environment variables into the correct format first
process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';

// Set some defaults
process.traceDeprecation = true;

const resolve = require('./utils/resolve');
const webpack = require('webpack');
const glob = require('glob');

// Get all of our build configurations
let build_configs = glob.sync(resolve('src/**/webpack.config.js'));

// Validator for build configs, there are a few required properties
let validateConfig = ({ realpath, module }) => {
	// An entry property is necessary for a build
	if (Object.keys(module.entry).length === 0) {
		console.warn(`Invalid config at ${realpath}. Did you forget to specify an \`entry\` property?`);
		return false;
	}
	return true;
};

// require and merge all of our widget configs that are valid
let entries = build_configs
	.map(config_path => ({ realpath: config_path, module: require(config_path) }))
	.filter(validateConfig)
	.reduce((state, config) => Object.assign(state, config.module.entry), {});

// Generate our webpack config
let config = require(resolve('config/webpack.config.prod'))(entries);

// Create our compiler and run it
let compiler = webpack(config);

compiler.run((err, stats) => {
	if (err) { throw err; }

	console.log(stats.toString({
		errorDetails: true,
		warnings: true,
		modules: false,
		chunks: false,
		colors: true
	}));
});
