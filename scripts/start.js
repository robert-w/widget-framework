// Force the environment variables into the correct format first
process.env.BABEL_ENV = 'development';
process.env.NODE_ENV = 'development';

const getEnvironmentSettings = require('../config/env');
const WebpackDevServer = require('webpack-dev-server');
const resolve = require('./utils/resolve');
const webpack = require('webpack');
const glob = require('glob');

// Get all of our build configurations
let build_configs = glob.sync(resolve('src/**/webpack.config.js'));

// Grab environment settings, we will need the port from this
let { raw: env } = getEnvironmentSettings();

// Validator for build configs, there are a few required properties
// for local development, we need a filename and mount_point so we
// can generate an html file for testing, and an entry
let validateConfig = ({ realpath, module }) => {
	// An entry property is necessary for a build
	if (Object.keys(module.entry).length === 0) {
		console.error(`Invalid config at ${realpath}. Did you forget to specify an \`entry\` property?`);
		return false;
	}

	if (!module.public.filename) {
		console.error(`Invalid config at ${realpath}. Did you forget to specify an \`public.filename\` property?`);
		return false;
	}

	if (!module.public.mount_point) {
		console.error(`Invalid config at ${realpath}. Did you forget to specify an \`public.mount_point\` property?`);
		return false;
	}

	return true;
};

// require and merge all of our widget configs that are valid
let configs = build_configs
	.map(config_path => ({ realpath: config_path, module: require(config_path) }))
	.filter(validateConfig);

// Generate our webpack config
let config = require(resolve('config/webpack.config.dev'))(configs);

// Webpack Dev Server Configuration
let dev_server_config = require(resolve('config/webpack.dev-server.config'));

// Create our compiler
let compiler = webpack(config);

// Create our dev server
let server = new WebpackDevServer(compiler, dev_server_config);

// Startup our dev server
server.listen(env.PORT, '127.0.0.1', () => console.log(`\x1B[1mStarting server on \x1B[36mhttp://localhost:${env.PORT}\x1B[39m\x1B[22m`));
