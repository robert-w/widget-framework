const path = require('path');
const fs = require('fs');

let root_directory = fs.realpathSync(process.cwd());

module.exports = relative_path => path.posix.resolve(root_directory, relative_path);
