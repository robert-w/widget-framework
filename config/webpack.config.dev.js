const HtmlWebpackPlugin = require('html-webpack-plugin');
const resolve = require('../scripts/utils/resolve');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const path = require('path');

module.exports = (configs) => {

	// Grab our environment settings
	let { raw: env, webpack_env } = getEnvironmentSettings();

	// Merge all of our entries together
	let entries = configs.reduce((state, config) =>
		Object.assign(state, config.module.entry), {});

	// Add the webpack hot entry points
	Object.keys(entries).forEach(name => {
		entries[name] = [
			`webpack-dev-server/client?http://localhost:${ env.PORT || 3000 }`,
			'webpack/hot/dev-server',
			entries[name]
		];
	});

	let html_webpack_plugins = configs.map(config => {
		let module = config.module || {};
		let title = module.public.title || 'Development Widget';
		let chunks = Object.getOwnPropertyNames(module.entry);
		let filename = module.public.filename;
		let mount = module.public.mount_point;

		return new HtmlWebpackPlugin({
			template: resolve('config/webpack.dev-server.template.html'),
			chunks: chunks.concat('vendor'),
			inject: true,
			filename,
			mount,
			title
		});
	});

	return {
		mode: env.NODE_ENV,
		profile: true,
		entry: entries,
		output: {
			path: path.join(process.cwd(), 'public'),
			filename: '[name].[hash].js'
		},
		module: {
			rules: [{
				test: /\.js?$/,
				loader: 'babel-loader'
			}]
		},
		plugins: [
			new webpack.DefinePlugin(webpack_env),
			new webpack.optimize.OccurrenceOrderPlugin(),
			new webpack.HotModuleReplacementPlugin(),
			...html_webpack_plugins
		],
		optimization: {
			splitChunks: {
				cacheGroups: {
					vendor: {
						name: 'vendor',
						chunks: 'all',
						minChunks: 2
					}
				}
			}
		}
	};
};
