const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const path = require('path');

module.exports = entries => {

	// Grab our environment settings
	let { raw: env, webpack_env } = getEnvironmentSettings();

	return {
		mode: env.NODE_ENV,
		profile: true,
		entry: entries,
		output: {
			path: path.join(process.cwd(), 'build'),
			filename: '[name].[hash].js'
		},
		module: {
			rules: [{
				test: /\.js?$/,
				loader: 'babel-loader'
			}]
		},
		plugins: [
			new webpack.DefinePlugin(webpack_env),
			new webpack.optimize.OccurrenceOrderPlugin()
		],
		optimization: {
			splitChunks: {
				cacheGroups: {
					vendor: {
						name: 'vendor',
						chunks: 'all',
						minChunks: 2
					}
				}
			}
		}
	};
};
