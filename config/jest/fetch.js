// Mock fetch in jest so we can have more fine grain control
// over what happens when using fetch. When using this, you may
// want to use fetchPromiseResolution defined at the bottom. It
// can be used to wait for the mock fetch promise to resolve.
if (process.env.NODE_ENV === 'test') {
	let fetch_results,
			fetch_error;

	// Add some methods for setting the outcome of the promise
	let set_results = results => {
		fetch_results = results;
	};

	let set_error = error => {
		fetch_error = error;
	};

	let reset = () => {
		fetch_results = undefined;
		fetch_error = undefined;
	};
	// Helper method for returning JSON of request
	let response = (results) => ({
		json: () => results || {}
	});

	// Mock out the fetch function
	let fetch = () => new Promise((resolve, reject) => {
		if (fetch_results) { return resolve(response(fetch_results)); }
		if (fetch_error) { return reject(fetch_error); }
		return resolve(response());
	});

	// Attach our helper methods
	fetch.__set_results = set_results;
	fetch.__set_error = set_error;
	fetch.__reset = reset;

	// Make our fetch available
	window.fetch = fetch;

	// Promise's resolve on next tick, so when using the above
	// fetch polyfill for testing. For an example usage,
	// see src/Demo/Demo.test.js
	window.fetchPromiseResolution = () => new Promise((resolve, _) => setImmediate(resolve));

}
