// These polyfills are currently only to enhance Jest and make testing as smooth as possible.
// jsdom currently does not support raf, so polyfill that for testing.
if (process.env.NODE_ENV === 'test') {
	// Polyfill requestAnimationFrame
	require('raf').polyfill(global);
}
