const path = require('path');

module.exports = {
	contentBase: path.join(process.cwd(), 'public'),
	watchContentBase: true,
	compress: true,
	hot: true,
	stats: {
		performance: true,
		modules: false,
		colors: true,
		assets: true
	}
};
