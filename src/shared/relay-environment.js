import {
	RecordSource,
	Environment,
	Network,
	Store
} from 'relay-runtime';

function query (operation, variables) {
	return fetch('https://search-now.medstarhealth.org/graphql', {
		method: 'POST',
		headers: {
			'content-type': 'application/json',
			'accept': 'application/json'
		},
		body: JSON.stringify({
			query: operation.text,
			variables
		})
	}).then(res => res.json());
}

let environment = new Environment({
	store: new Store(new RecordSource()),
	network: Network.create(query)
});

export default environment;
