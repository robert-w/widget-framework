import getDefaultState from './getDefaultState';

describe('Function: getDefaultState', () => {

	beforeEach(() => {
		window.DEFAULT_STATE = undefined;
		window.CUSTOM_VARIABLE = undefined;
	});

	test('should return an empty object if no state is defined and no props are given', () => {
		expect(Object.keys(getDefaultState())).toHaveLength(0);
	});

	test('should return default state from the window variable', () => {
		let default_state = { south: 'park', coon: 'cartman' };
		window.DEFAULT_STATE = default_state;

		let state = getDefaultState();
		expect(state).toBe(default_state);
	});

	test('should return default state from a custom variable', () => {
		let default_state = { south: 'park', coon: 'cartman' };
		window.CUSTOM_VARIABLE = default_state;

		let state = getDefaultState({ global_name: 'CUSTOM_VARIABLE' });
		expect(state).toBe(default_state);
	});

	test('should return default state from an element', () => {
		let div = document.createElement('div');
		div.setAttribute('data-scooby', 'doo');
		div.setAttribute('data-shaggy', 'too');
		div.setAttribute('data-mystery-machine', 'volkswagen');

		let state = getDefaultState({ element: div });
		expect(state.scooby).toEqual('doo');
		expect(state.shaggy).toEqual('too');
		expect(state.mysteryMachine).toEqual('volkswagen');
	});

	test('should return default state from element_id', () => {
		let div = document.createElement('div');
		div.setAttribute('id', 'widget-root');
		div.setAttribute('data-scooby', 'doo');
		div.setAttribute('data-shaggy', 'too');
		div.setAttribute('data-mystery-machine', 'volkswagen');
		document.body.appendChild(div);

		let state = getDefaultState({ element_id: 'widget-root' });
		expect(state.scooby).toEqual('doo');
		expect(state.shaggy).toEqual('too');
		expect(state.mysteryMachine).toEqual('volkswagen');
	});

	test('should return a filterd default state when given properties', () => {
		let default_state = { south: 'park', mysterion: 'kenny', foo: 'bar' };
		window.DEFAULT_STATE = default_state;

		let state = getDefaultState({
			properties: [ 'south', 'mysterion' ]
		});

		expect(state.south).toEqual('park');
		expect(state.mysterion).toEqual('kenny');
		expect(state.foo).toBeUndefined();
	});

	test('should return a combined default state from a custom variable and an element', () => {
		let div = document.createElement('div');
		div.setAttribute('data-scooby', 'doo');
		div.setAttribute('data-shaggy', 'too');
		div.setAttribute('data-mystery-machine', 'volkswagen');

		let additional_state = { south: 'park', mysterion: 'kenny' };
		window.CUSTOM_VARIABLE = additional_state;

		let state = getDefaultState({
			properties: [ 'mysterion', 'scooby', 'shaggy' ],
			global_name: 'CUSTOM_VARIABLE',
			element: div
		});

		expect(state.scooby).toEqual('doo');
		expect(state.shaggy).toEqual('too');
		expect(state.mysterion).toEqual('kenny');
		expect(state.south).toBeUndefined();
		expect(state.mysteryMachine).toBeUndefined();
	});

});
