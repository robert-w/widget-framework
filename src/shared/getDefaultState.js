// DISCLAIMER: This uses DOMStringMap, see
// https://developer.mozilla.org/en-US/docs/Web/API/DOMStringMap
// This is accessed via element.dataset and is only compatible with
// IE11 and up, if we need to support older browsers, the logic will
// need to be tweaked to use getAttribute instead. Using element.dataset
// does transform element property names from foo-bar to fooBar while
// getAttribute does not.

/**
 * @description Helper function to return only the interested fields
 * from an object or the whole object if fields is empty
 * @param {Array<string>} properties - an array of property names
 * @param {object} item - object to filter
 * @return {object}
 */
function copyObject (properties, item) {
	// if we have no properties, return the object as is
	if (!properties.length) {
		return item;
	}
	// if we have properties, only return properties we are interested in
	return properties.reduce((obj, field_name) => {
		obj[field_name] = item[field_name];
		return obj;
	}, {});
}


/**
 * @description This function will attempt to parse the default state for
 * a widget from the hosting page. It will attempt to parse properties
 * from multiple locations. Either a global variable named DEFAULT_STATE
 * or from data-properties on the root element. If no properties are given,
 * all available data-properties or properties on global vars will be
 * passed back to you.
 * @param {object} options - Options object
 * @param {Array<string>} properties - array of properties to return
 * @param {string} global_name - global variable to find state on
 * @param {HTMLElement} element - The root node the widget has mounted to.
 * You can access this in your component by calling `ReactDOM.findDOMNode(this)`
 * in your root component class. See the Demo widget for an example.
 * @param {string} element_id - Id of the element to get data-properties from
 * @return {Object} - Key value pair of properties found
 */
module.exports = function getDefaultState (options = {}) {
	let default_state = {};
	let {
		element,
		element_id,
		properties = [],
		global_name = 'DEFAULT_STATE',
	} = options;

	// If we have the variables on the window object, get those properties now
	// don't return yet incase some properties are specified via data-props
	if (window[global_name]) {
		default_state = copyObject(properties, window[global_name]);
	}

	// If we have an element, let's parse the properties from that into an object
	element = element || (element_id && document.getElementById(element_id));

	if (element && element.dataset) {
		default_state = copyObject(
			properties,
			Object.assign(default_state, element.dataset)
		);
	}

	return default_state;
};
