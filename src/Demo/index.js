import getDefaultState from '../shared/getDefaultState';
import ReactDOM from 'react-dom';
import React from 'react';
import Demo from './Demo';

// Test Load via global variables
window.DEFAULT_STATE = {
	message: 'Hello Demo'
};

let default_state = getDefaultState({
	properties: [ 'message' ],
	// use this property if the dom looks like this
	// <div id='demo-widget' data-message='Hello Demo'></div>
	// element_id: 'demo-widget'
});

ReactDOM.render(
	<Demo message={default_state.message} />,
	document.getElementById('demo-widget')
);

/* istanbul ignore next */
if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./Demo', () => {
    let HotApp = require('./Demo').default;
    ReactDOM.render(<HotApp />, document.getElementById('demo-widget'));
  });
}
