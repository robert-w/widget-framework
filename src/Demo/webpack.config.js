const path = require('path');

// This config is used by webpack to help build and generate the bundles.
// This project will iterate over every folder in the `src` directory and
// add a new entry for every webpack.config.js it finds with an entry
// property. It will also generate a HTML page for you to view the
// widget at so you can test it as you go.
module.exports = {
	// Please make sure to include src in your path
	entry: {
		// This key should be unique across all widgets in your project, meaning you can't have
		// two widgets with the same name because when these configs get merged, one will be overwritten.
		demo: path.posix.resolve('./src/Demo/index.js')
	},
	// This is the public configurations for the demo widget will be available at for testing.
	// This only matters for local development.
	public: {
		// The filename is the name of the html file that get's generated
		// Keep this in root so HMR works
		filename: 'demo.html',
		// The mount point is the id in the dom this component will attempt to mount to.
		// Ideally try to make these different so that if you are developing multiple widgets
		// for the same website, someone could use multiple widgets on a single page. If they
		// all mount to the same dom node, that is impossible
		mount_point: 'demo-widget'
	}
};
