import { empty } from './__mocks__/query-results';

describe('Demo Entry Point', () => {

	beforeEach(() => {
		// This is a fake empty set of data
		window.fetch.__set_results(empty);
	});

	afterEach(() => {
		window.fetch.__reset();
	});

	test('should load without throwing an error', () => {
		let div = document.createElement('div');
		div.setAttribute('id', 'demo-widget');
		document.body.appendChild(div);
		require('./index');
	});

});
