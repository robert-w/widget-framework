// Empty query results
export const empty = {
	data: {
		provider_search: {
			providers: []
		}
	}
};

export const threeProviders = {
	data: {
		provider_search: {
			providers: [
				{
					name: { full_name: 'John Doe' }
				},
				{
					name: { full_name: 'Jane Doe' }
				},
				{
					name: { full_name: 'Joe Doe' }
				}
			]
		}
	}
};
