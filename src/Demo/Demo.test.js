import { empty, threeProviders } from './__mocks__/query-results';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import React from 'react';
import Demo from './Demo';

describe('Demo Component', () => {

	beforeEach(() => {
		window.fetch.__reset();
	});

	afterAll(() => {
		window.fetch.__reset();
	});

	test('should render without throwing an error', () => {
		let div = document.createElement('div');
		ReactDOM.render(<Demo />, div);
		ReactDOM.unmountComponentAtNode(div);
	});

	test('should initially render a loading message', () => {
		// Set empty results to prevent a relay network issue
		window.fetch.__set_results(empty);
		// Mount our component and look for the appropriate message
		let demo = mount(<Demo />);
		let text = demo.find('.loading').text();
		// Because we are not waiting for the fetch promise to resolve
		// we should sitll see the loading message
		expect(text).toEqual('Loading Query..');
	});

	test('should render an error message when fetch fails', async () => {
		// set fetch to error out
		window.fetch.__set_error(new Error('Network Error'));
		// Mount our component and look for the appropriate message
		let demo = mount(<Demo />);
		// await our fetch promise resolution
		await window.fetchPromiseResolution();
		// Force a re-render
		demo.update();
		// Look for the error message
		let text = demo.find('.error').text();
		expect(text).toEqual('Error: Network Error');
	});

	test('should render three providers on a successful query', async () => {
		// Set our results
		window.fetch.__set_results(threeProviders);
		let demo = mount(<Demo />);
		// await our mock query to resolve
		await window.fetchPromiseResolution();
		// Force a re-render
		demo.update();
		// Look for our three providers
		let providers = demo.find('.providers').children();
		expect(providers).toHaveLength(3);
	});

});
