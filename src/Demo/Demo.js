import environment from '../shared/relay-environment';
import { graphql, QueryRenderer } from 'react-relay';
import ProviderList from './ProviderList';
import React from 'react';

let query = graphql`
	query DemoProviderQuery($size: Int) {
		provider_search(size: $size) {
			...ProviderList_providerData
		}
	}
`;

let variables = {
	size: 3
};

export default class Demo extends React.Component {

	render () {
		return (
			<QueryRenderer
				environment={environment}
				variables={variables}
				query={query}
				render={({error, props}) => {
					if (error) { return <div className='error'>Error: { error.message }</div>; }
					if (!props) { return <div className='loading'>Loading Query..</div>; }

					return (
						<ProviderList providerData={props.provider_search} />
					);
				}}
			/>
		);
	}

}
