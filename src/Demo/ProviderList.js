import { graphql, createFragmentContainer } from 'react-relay';
import React from 'react';

class ProviderList extends React.Component {
	render () {
		let { providerData: { providers }} = this.props;

		return (
			<div className='providers'>
				{providers.map(provider => {
					return (
						<div key={provider.name.full_name}>
							{ provider.name.full_name }
						</div>
					);
				})}
			</div>
		);
	}
}

export default createFragmentContainer(
	ProviderList,
	graphql`
		fragment ProviderList_providerData on ProviderSearchResult {
			providers {
				name {
					full_name
				}
			}
		}
	`
);
