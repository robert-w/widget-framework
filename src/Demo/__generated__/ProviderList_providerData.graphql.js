/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteFragment } from 'relay-runtime';
import type { FragmentReference } from 'relay-runtime';
declare export opaque type ProviderList_providerData$ref: FragmentReference;
export type ProviderList_providerData = {|
  +providers: ?$ReadOnlyArray<?{|
    +name: ?{|
      +full_name: ?string,
    |},
  |}>,
  +$refType: ProviderList_providerData$ref,
|};
*/


const node/*: ConcreteFragment*/ = {
  "kind": "Fragment",
  "name": "ProviderList_providerData",
  "type": "ProviderSearchResult",
  "metadata": null,
  "argumentDefinitions": [],
  "selections": [
    {
      "kind": "LinkedField",
      "alias": null,
      "name": "providers",
      "storageKey": null,
      "args": null,
      "concreteType": "ProviderSchema",
      "plural": true,
      "selections": [
        {
          "kind": "LinkedField",
          "alias": null,
          "name": "name",
          "storageKey": null,
          "args": null,
          "concreteType": "ProviderNameSchema",
          "plural": false,
          "selections": [
            {
              "kind": "ScalarField",
              "alias": null,
              "name": "full_name",
              "args": null,
              "storageKey": null
            }
          ]
        }
      ]
    }
  ]
};
(node/*: any*/).hash = '9ad51521a12e0f8912d00b7442891c1e';
module.exports = node;
