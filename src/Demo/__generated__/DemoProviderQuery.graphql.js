/**
 * @flow
 * @relayHash 33d6150e7d0ab94cd22552ea9ccd2e3c
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
type ProviderList_providerData$ref = any;
export type DemoProviderQueryVariables = {|
  size?: ?number,
|};
export type DemoProviderQueryResponse = {|
  +provider_search: ?{|
    +$fragmentRefs: ProviderList_providerData$ref,
  |},
|};
*/


/*
query DemoProviderQuery(
  $size: Int
) {
  provider_search(size: $size) {
    ...ProviderList_providerData
  }
}

fragment ProviderList_providerData on ProviderSearchResult {
  providers {
    name {
      full_name
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "size",
    "type": "Int",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "size",
    "variableName": "size",
    "type": "Int"
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "DemoProviderQuery",
  "id": null,
  "text": "query DemoProviderQuery(\n  $size: Int\n) {\n  provider_search(size: $size) {\n    ...ProviderList_providerData\n  }\n}\n\nfragment ProviderList_providerData on ProviderSearchResult {\n  providers {\n    name {\n      full_name\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "DemoProviderQuery",
    "type": "Query",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "provider_search",
        "storageKey": null,
        "args": v1,
        "concreteType": "ProviderSearchResult",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "ProviderList_providerData",
            "args": null
          }
        ]
      }
    ]
  },
  "operation": {
    "kind": "Operation",
    "name": "DemoProviderQuery",
    "argumentDefinitions": v0,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "provider_search",
        "storageKey": null,
        "args": v1,
        "concreteType": "ProviderSearchResult",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "providers",
            "storageKey": null,
            "args": null,
            "concreteType": "ProviderSchema",
            "plural": true,
            "selections": [
              {
                "kind": "LinkedField",
                "alias": null,
                "name": "name",
                "storageKey": null,
                "args": null,
                "concreteType": "ProviderNameSchema",
                "plural": false,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "name": "full_name",
                    "args": null,
                    "storageKey": null
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
};
})();
(node/*: any*/).hash = '81de4b96213334c3d6584663c37e6936';
module.exports = node;
